package Musicbox;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Musicbox {
    private String name;

    private Record record;
    private ArrayList<Record> records = new ArrayList(50);

    private Record playingRecord;
    private double sumOfMusic = 0;
    private int capacity = 50;


    public Musicbox(String name) {
        this.name = name;
    }

    public void addRecord(Record record) {
        if (this.capacity > 0) {
            this.capacity = capacity - 1;
            this.record = record;
            this.records.add(record);
        } else {
            System.out.println("Error");
        }
    }

    public void removeRecord(Record record) {
        boolean removeRecord = false;
        for (Record record2 : records) {
            if (record == record2) {
                removeRecord = true;
            }
        }
        if (removeRecord) {
            this.records.remove(record);
            this.capacity = capacity + 1;
        } else {
            System.out.println("There isn't such a record");
        }

    }

    public void loadRecord(Record record) {
        boolean loadRecord = false;
        for (Record record2 : records) {
            if (record == record2) {
                loadRecord = true;
            }
        }
        if (loadRecord) {
            this.playingRecord = record;
        } else {
            System.out.println("There isn't such a record");
        }

    }

    public void playRecord() {
        if (playingRecord != null) {
            System.out.println("Playing now " + playingRecord.getName());
            //System.out.println(playingRecord.getPlayTime());
            this.sumOfMusic = sumOfMusic + playingRecord.getPlayTime();
        } else {
            System.out.println("Select a record before");
        }
    }

    public void playSong(int i) {
        i = i - 1;
        if (i > record.songs.size()) {
            System.out.println("This number is not existing");
        } else {
            if (playingRecord != null) {
                System.out.println("Playing now " + playingRecord.songs.get(i).getName());
                //System.out.println(playingRecord.getPlayTime());
                this.sumOfMusic = sumOfMusic + playingRecord.songs.get(i).getSongDuration();
            } else {
                System.out.println("Select a record before");
            }
        }
    }


    public void showRecords() {
        for (int i = 0; i < records.size(); i++) {
            System.out.println("Name: " + this.records.get(i).getName());
            System.out.println("Duration: " + this.records.get(i).getPlayTime());
        }
    }

    public void showSongs(Record record) {
        for (int i = 0; i < record.songs.size(); i++) {
            System.out.println("hallo");
            System.out.println("Name: " + record.songs.get(i).getName());
            System.out.println("Duration: " + record.songs.get(i).getSongDuration());
            System.out.println("Number: " + (i + 1));
        }
    }

    public void searchRecord(String title) {
        boolean error = true;
        for (Record record : records) {
            for (Song song : record.songs) {
                if (title == song.getName()) {
                    System.out.println("Record name: " + record.getName());
                    System.out.println("Song number: " + (record.songs.indexOf(song) + 1));
                    error = false;
                }
            }
        }
        if (error) {
            System.out.println("There isn't a song like this");
        }
    }

    public void getSumOfMusic() {
        DecimalFormat f = new DecimalFormat("#0.0");
        System.out.println("Time played Music: " + f.format(sumOfMusic));
    }


}
