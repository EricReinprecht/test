package Musicbox;

import java.util.ArrayList;

public class Record {
    private String name;
    private int id;
    public ArrayList<Song> songs = new ArrayList();
    double playTime;


    public Record(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public void addSong(Song song) {
        this.songs.add(song);
    }

    public String getName() {
        return name;
    }


    public void getRecordDuration() {
        double playTime = 0;
        for (Song song : songs) {
            playTime = playTime + song.getSongDuration();
        }
        this.playTime = playTime;
    }

    public double getPlayTime() {
        getRecordDuration();
        return playTime;
    }
}