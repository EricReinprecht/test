package Musicbox;

public class Song {
    private String name;
    private double playTime;

    public Song(String name, double playTime) {
        this.name = name;
        this.playTime = playTime;
    }

    public String getName() {
        return name;
    }

    public double getSongDuration() {
        return playTime;
    }
}