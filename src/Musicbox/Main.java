package Musicbox;

public class Main {
    public static void main(String[] args) {

        Record d1 = new Record("Summer", 4);
        Record d2 = new Record("Hello", 5);
        Record d3 = new Record("Sunshine", 17);

        Song s2 = new Song("Thunderstruck", 3.2);
        Song s1 = new Song("Rockstar", 2.2);
        Song s3 = new Song("Wälderbähnle", 5);

        Musicbox m1 = new Musicbox("Wurlitzer");

        d1.addSong(s1);
        d1.addSong(s2);

        m1.addRecord(d1);
        m1.addRecord(d2);
        m1.removeRecord(d2);
        m1.removeRecord(d3);  //ERROR

        m1.playRecord();    //ERROR
        m1.loadRecord(d1);
        m1.playRecord();
        m1.playRecord();
        m1.playRecord();

        m1.playSong(1);
        m1.playSong(34);

        m1.searchRecord("Rockstar");
        m1.searchRecord("Thunderstruck");
        m1.searchRecord("Wälderbähnle");   //ERROR

        m1.showSongs(d1);
        m1.showRecords();

        m1.getSumOfMusic();
    }
}